FROM python:3.11

RUN pip3 install --upgrade pip

ADD requirements.txt fastapp/

RUN pip install -r fastapp/requirements.txt

COPY fastapp/ /fastapp

EXPOSE 5001