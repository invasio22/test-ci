from sqlalchemy import Column, Integer, String, Text

from fastapp.database import Base


class Recipe(Base):
    __tablename__ = "recipe"
    id = Column(Integer, primary_key=True, index=True, unique=True)
    title = Column(String, index=True)
    count_views = Column(
        Integer,
    )
    cooking_time = Column(
        Integer,
    )
    ingredient_list = Column(
        Text,
    )
    description = Column(Text)

    def __repr__(self):
        return f"{self.title}"
