from typing import List

import uvicorn
from fastapi import FastAPI, HTTPException
from fastapi.encoders import jsonable_encoder
from sqlalchemy import delete, select, update

from fastapp import schemas
from fastapp.database import engine, session
from fastapp.models import Base, Recipe

app = FastAPI()


@app.on_event("startup")
async def startup():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


@app.on_event("shutdown")
async def shutdown():
    await session.close()
    await engine.dispose()


@app.get("/recipes", response_model=List[schemas.RecipeOut], status_code=200)
@app.get("/recipes/{item_id}", status_code=200)
async def get_recipe(
    item_id: int | None = None,
):
    async with session.begin():
        res = await session.execute(select(Recipe))
        if item_id:
            res = await session.execute(
                select(Recipe).filter(Recipe.id == item_id)
            )
        return res.scalars().all()


@app.post("/recipes", response_model=schemas.RecipeOut, status_code=201)
async def post_recipe(recipe: schemas.RecipeIn) -> Recipe:
    new_recipe = Recipe(**recipe.dict())
    async with session.begin():
        session.add(new_recipe)
        await session.commit()
    return new_recipe


@app.put("/recipes/{item_id}", status_code=200)
async def update_recipe(item_id: int, item: schemas.RecipeIn) -> None:
    res = jsonable_encoder(item)
    async with session.begin():
        await session.execute(
            update(Recipe).where(Recipe.id == item_id).values(**item.dict())
        )
        await session.commit()
    return res


@app.delete("/recipes/{item_id}", status_code=200)
async def delete_recipe(item_id: int):
    async with session.begin():
        try:
            await session.execute(delete(Recipe).where(Recipe.id == item_id))
        except Exception:
            raise HTTPException(status_code=404, detail="Item not found")
    return {"message": "Delete complete"}


if __name__ == "__main__":
    uvicorn.run("main:app", port=5001)
