from pydantic import BaseModel


class BaseRecipe(BaseModel):
    title: str
    count_views: int = 0
    cooking_time: int
    ingredient_list: str
    description: str | None = None


class RecipeIn(BaseRecipe):
    ...


class RecipeOut(BaseRecipe):
    id: int | None = None

    class Config:
        orm_mode = True
