import asyncio
from typing import AsyncGenerator
import pytest_asyncio
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import (
    AsyncSession,
    create_async_engine,
)
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import NullPool

from fastapp.database import Base, get_db
from fastapp.main import app
from fastapp.models import Recipe
from fastapp.src.config import (
    DB_HOST_TEST,
    DB_NAME_TEST,
    DB_PASS_TEST,
    DB_PORT_TEST,
    DB_USER_TEST,
)

# DATABASE
TEST_DATABASE = (
    f"postgresql+asyncpg:"
    f"//{DB_USER_TEST}:"
    f"{DB_PASS_TEST}@{DB_HOST_TEST}:{DB_PORT_TEST}/{DB_NAME_TEST}"
)

engine_test = create_async_engine(TEST_DATABASE, poolclass=NullPool)
async_session_maker = sessionmaker(
    engine_test, class_=AsyncSession, expire_on_commit=False
)


async def override_get_async_session() -> AsyncGenerator[AsyncSession, None]:
    async with async_session_maker() as session:
        yield session


app.dependency_overrides[get_db] = override_get_async_session


@pytest_asyncio.fixture(autouse=True, scope="session")
async def prepare_database():
    async with engine_test.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
    yield
    async with engine_test.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)


# SETUP
@pytest_asyncio.fixture(scope="session")
def event_loop(request):
    """Create an instance of the default event loop for each test case."""
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


# client = TestClient(app)


@pytest_asyncio.fixture(scope="session")
async def client() -> AsyncGenerator[AsyncClient, None]:
    async with AsyncClient(
        app=app, base_url="http://0.0.0.0:5001/"
    ) as async_client:
        yield async_client


@pytest_asyncio.fixture(scope="function")
async def new_recipe():
    async with get_db() as conn:
        item = Recipe(
            title="Test",
            cooking_time=14,
            ingredient_list="123",
            description="321",
        )
        yield item
        await item.delete(conn)
