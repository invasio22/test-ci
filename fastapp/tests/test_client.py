import pytest
from httpx import AsyncClient


@pytest.mark.asyncio
async def test_get_user(event_loop, client: AsyncClient):
    response = await client.get("/recipes")
    assert response.status_code == 200
#

# class TestAPI:
#     @pytest.mark.asyncio
#     async def test_client(self, client: AsyncClient):
#         response = await client.get('/recipes')
#         assert response.status_code == 200
#
#     @pytest.mark.asyncio
#     async def test_post_recipe(self, new_recipe, client: AsyncClient):
#         response = await client.post(
#             "/recipes",
#             json=new_recipe.dict()
#         )
#         assert response.status_code == 201
#
#     @pytest.mark.asyncio
#     async def test_get_recipe(self, client: AsyncClient, new_recipe):
#         response = await client.get("/recipes/1")
#         assert response.status_code == 200
#         assert response.json() == [new_recipe.dict()]
#
#     @pytest.mark.asyncio
#     async def test_put_recipe(self, client: AsyncClient):
#         response = await client.put("/recipes/1", json={
#             "id": 1,
#             "title": "Test1",
#             "count_views": 0,
#             "cooking_time": 14,
#             "ingredient_list": "123",
#             "description": "321"
#         })
#         assert response.status_code == 200
#         assert response.json() == {
#             "title": "Test1",
#             "count_views": 0,
#             "cooking_time": 14,
#             "ingredient_list": "123",
#             "description": "321"
#         }
#
#     @pytest.mark.asyncio
#     async def test_delete_recipe(self, client: AsyncClient):
#         response = await client.delete("/recipes/1")
#         assert response.status_code == 200
#         assert response.json() == {'message': 'Delete complete'}
